import { Service, Errors } from "moleculer";
import * as Sequelize from "sequelize";
import * as moment from "moment";
import * as yn from "yn";
import * as path from "path";

class ModmailAlertsService extends Service {
	protected db: Sequelize.Sequelize;
	protected SlackAlert: Sequelize.Model<{channel: string, ts: string}, {}>

	constructor (broker) {
		super(broker);

		this.parseServiceSchema({
			name: "bot.modmailAlert",
			version: 1,
			dependencies: [
				{name: "reddit.post", version: 1},
				{name: "reddit.modmail", version: 1},
				{name: "slack.web", version: 1}
			],
			actions: {
				checkModmail: {
					name: "checkModmail",
					handler: this.checkModmail
				}
			},
			// @ts-ignore (bug introduced in v0.13)
			started: this.serviceStarted
		});
	}

	async checkModmail() {
		let modmail;
		try{
			modmail = await this.broker.call("v1.reddit.modmail.getConversations", {
				entity: process.env.BOT_MODMAILALERT_SUBREDDIT,
				limit: 100,
				state: "all"
			});
		} catch (err) {
			this.logger.error("Reddit call returned error, please investigate. Restarting in 2 minutes...");
			setTimeout(() => this.broker.call("v1.bot.modmailAlert.checkModmail"), 2*60*1000);
			return;
		}

		let unanswered = 0;

		for (let conversationId in modmail.conversations) {
			let mail = modmail.conversations[conversationId];
			if (mail.isInternal || mail.isAuto) continue;

			if (mail.lastUpdated === mail.lastUserUpdate || mail.authors[0].name === "PCMRBot") unanswered++;
		}

		if (unanswered >= (Number(process.env.BOT_MODMAILALERT_THRESHOLD) || 15)) {
			this.postSlackAlert(unanswered);
		} else {
			this.removeSlackAlert();
		}

		setTimeout(() => this.broker.call("v1.bot.modmailAlert.checkModmail"), 2*60*1000);
	}	

	async postSlackAlert(unresolvedModmail: number) {
		let existingAlert = await this.SlackAlert.findOne();
		if(existingAlert === null) {
			this.logger.debug("Slack alert does not exist, creating...");
			
			let messageString = `*Notice* <!channel>: There are ${unresolvedModmail} modmail messages that need to be replied to`;
			
			let message = await this.broker.call("v1.slack.web.postMessage", {
				channel: process.env.BOT_MODMAILALERT_CHANNEL,
				message: messageString
			});
			
			await this.SlackAlert.create({
				channel: message.channel,
				ts: message.ts
			});
		} else {
			this.logger.debug("Slack alert already exists, updating...");
			let messageString = `*Notice* @channel: There are ${unresolvedModmail} modmail messages that need to be replied to`;
			
			await this.broker.call("v1.slack.web.updateMessage", {
				channel: existingAlert.channel,
				ts: existingAlert.ts,
				message: messageString
			});
		}
	}
	
	async removeSlackAlert() {
		let existingAlert = await this.SlackAlert.findOne();
		if (existingAlert === null) {
			this.logger.debug("No action needed");
		} else {
			this.logger.debug("Slack alert exists, deleting...");
			
			let time = moment.utc();
			await this.broker.call("v1.slack.web.updateMessage", {
				channel: existingAlert.channel,
				ts: existingAlert.ts,
				message: `Modmail unanswered count fell below threshold at <!date^${time.unix()}^{time} on {date}|${time.format("LTS")} UTC on ${time.format("LL")}>`
			});

			await this.SlackAlert.destroy({where: {ts: existingAlert.ts}})
		}
	}

	async serviceStarted() {
		if(process.env.BOT_MODMAILALERT_CHANNEL === "") throw new Errors.MoleculerError("No modmail alert channel defined, you dingus");

		this.db = new Sequelize({
			dialect: process.env.BOT_MODMAILALERT_DB_DIALECT || undefined,
			database: process.env.BOT_MODMAILALERT_DB_NAME || undefined,
			host: process.env.BOT_MODMAILALERT_DB_HOST || undefined,
			username: process.env.BOT_MODMAILALERT_DB_USERNAME || undefined,
			password: process.env.BOT_MODMAILALERT_DB_PASSWORD || undefined,
			port: Number(process.env.BOT_MODMAILALERT_DB_PORT) || undefined,
			storage: process.env.BOT_MODMAILALERT_DB_PATH || undefined,
			operatorsAliases: false,
			logging: false
		});

		this.SlackAlert = this.db.import(path.join(__dirname, "./modmailSlackAlert.model"));

		await this.db.sync({force: yn(process.env.BOT_MODMAILALERT_DB_DROP_TABLES_ON_START, {default: false})});

		setTimeout(() => this.broker.call("v1.bot.modmailAlert.checkModmail"), 40*1000);
	}
}

module.exports = ModmailAlertsService;