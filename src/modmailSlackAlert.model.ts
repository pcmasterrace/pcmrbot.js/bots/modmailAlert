import * as Sequelize from "sequelize";

module.exports = function(sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes) {
	return sequelize.define("ModmailSlackAlert", {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		channel: DataTypes.STRING,
		ts: DataTypes.STRING
	});
}